﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xrm.SourceControl.Data;
using Xrm.SourceControl.Data.Models.SourceAnalyze;
using Xrm.SourceControl.SourceAnalyze;
using Xrm.SourceControl.SourceAnalyze.Annotations;
using Xrm.SourceControl.SourceAnalyze.Store;

namespace Xrm.SourceControl.App
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("usage: xrm.sourcecontrol.exe <path to dll>");
                return;
            }

            var pathToDll = args[0];
            if (!File.Exists(pathToDll))
            {
                Console.WriteLine("File not exist");
                return;
            }

            try
            {
                var assemblyAnalyzerFactory = new AssemblyAnalyzerFactory();
                var assemblyAnalyzer = assemblyAnalyzerFactory.Create<SourceAnnotation,SourceAnnotationAttribute>(cfg =>
                {
                    cfg.UseAnnotationFactory<SourceAnnotationFactory>();
                    cfg.UseFileAssemblyResolver(pathToDll);
                    cfg.UseDataStore<SourceAnnotationDataStore>();
                });

                assemblyAnalyzer.InsertNewAnnotations(AttributeTargets.Class | AttributeTargets.Method);

                var allAnnotations = (IEnumerable<SourceAnnotation>)null;
                using (var context = new DataContext())
                {
                    allAnnotations = context.SourceAnnotations.
                        OrderBy(o => o.Date)
                        .ToList();
                }

                WriteToConsole(allAnnotations);
            }
            catch (FileLoadException ex)
            {
                Console.WriteLine("Cannot load provided file");
            }
            catch (BadImageFormatException ex)
            {
                Console.WriteLine("Bad image format");
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("File not found");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something goes wrong =(");
            }
        }

        private static void WriteToConsole(IEnumerable<SourceAnnotation> annotations)
        {
            if (!annotations.Any())
                return;

            var @break = false;
            while (!@break)
            {
                foreach (var item in annotations)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;

                    Console.WriteLine("{0,-5} - {1,-5} - {2,-5} \n {3,-5} - {4,-5} - {5,-5}",
                        item.Author,
                        item.Date,
                        item.Reason,
                        item.AssemblyName,
                        item.MemberType,
                        item.MemberName);

                    if (Console.CursorTop > Console.WindowHeight - 5)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Press 'Enter' for continue or 'q' for exit");
                        var key = Console.ReadKey();

                        if (key.Key == ConsoleKey.Enter)
                        {
                            Console.Clear();
                            Console.SetCursorPosition(0, 0);
                        }
                        else if (key.Key == ConsoleKey.Q)
                        {
                            @break = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}

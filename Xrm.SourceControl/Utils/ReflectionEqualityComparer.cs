﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Xrm.SourceControl.Utils
{
    public class ReflectionEqualityComparer<T> : IEqualityComparer<T> where T : class
    {
        private IEnumerable<string> exceptPropertyNames;

        public ReflectionEqualityComparer(IEnumerable<string> exceptPropertyNames = null)
        {
            this.exceptPropertyNames = exceptPropertyNames;
        }

        public bool Equals(T x, T y)
        {
            if (x != null && y == null)
                return false;
            else if (x == null && y != null)
                return false;
            if (x == null && y == null)
                return true;


            var type = x.GetType();

            foreach (var propertyDescriptor in TypeDescriptor.GetProperties(type)
                .OfType<PropertyDescriptor>())
            {
                if (exceptPropertyNames != null && exceptPropertyNames.Contains(propertyDescriptor.Name))
                    continue;

                var xValue = propertyDescriptor.GetValue(x);
                var yValue = propertyDescriptor.GetValue(y);

                if (xValue == yValue)
                    continue;

                return false;
            }

            return true;
        }

        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }
}

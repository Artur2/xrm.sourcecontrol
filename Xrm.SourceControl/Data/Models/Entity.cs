﻿using System.Diagnostics;
using Xrm.SourceControl.Utils;

namespace Xrm.SourceControl.Data.Models
{
    [DebuggerDisplay("{TypeName,nq}={Id,nq}")]
    public class Entity
    {
        public int Id { get; set; }

        public virtual string TypeName
        {
            get
            {
                return GetType().Name;
            }
        }

        public override bool Equals(object obj)
        {
            var equalityComparer = new ReflectionEqualityComparer<object>(new[] { "Id" });
            return equalityComparer.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            var equalityComparer = new ReflectionEqualityComparer<object>(new[] { "Id" });
            return equalityComparer.GetHashCode(this);
        }
    }
}

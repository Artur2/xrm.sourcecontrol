﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Xrm.SourceControl.Data.Models.SourceAnalyze
{
    public class SourceAnnotation : Entity
    {
        [StringLength(30)]
        public string Author { get; set; }

        public DateTime Date { get; set; }

        public string Reason { get; set; }

        [StringLength(30)]
        public string MemberName { get; set; }

        [StringLength(50)]
        public string AssemblyName { get; set; }

        public SourceAnnotationMemberType MemberType { get; set; }
    }
}

﻿namespace Xrm.SourceControl.Data.Models.SourceAnalyze
{
    public enum SourceAnnotationMemberType
    {
        None = 0,
        Class,
        Method
    }
}

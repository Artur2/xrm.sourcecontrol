﻿using System.Data.Entity;
using System.Linq;
using Xrm.SourceControl.Data.Configuration;
using Xrm.SourceControl.Data.Models;
using Xrm.SourceControl.Data.Models.SourceAnalyze;

namespace Xrm.SourceControl.Data
{
    public class DataContext : DbContext
    {
        static DataContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, MigrateToLatestVersionConfiguration>());
        }

        //Data Source=(localdb)\mssqllocaldb;Initial Catalog=Xrm.SourceControl.Data.DataContext;Integrated Security=True;MultipleActiveResultSets=True

        public IDbSet<SourceAnnotation> SourceAnnotations
        {
            get
            {
                return Set<SourceAnnotation>();
            }
        }

        public Entity ThrowSeed()
        {
            return SourceAnnotations.FirstOrDefault();
        }
    }
}

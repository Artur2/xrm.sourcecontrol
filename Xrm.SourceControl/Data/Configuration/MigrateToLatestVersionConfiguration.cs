﻿using System.Data.Entity.Migrations;

namespace Xrm.SourceControl.Data.Configuration
{
    public class MigrateToLatestVersionConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrateToLatestVersionConfiguration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataContext context)
        {
            var dropView = @"IF EXISTS(SELECT * FROM sys.views v WHERE v.name = 'SourceAnnotationView')
                             DROP VIEW dbo.SourceAnnotationView;";

            var createView = @"CREATE VIEW SourceAnnotationView
                                WITH SCHEMABINDING
                                AS
                                SELECT sa.Id, sa.Author, sa.Date, sa.Reason, sa.MemberName, sa.MemberType, sa.AssemblyName 
                                FROM dbo.SourceAnnotations sa;";

            var createIndex = @"IF NOT EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'SourceAnnotationView_CIX')
                                CREATE UNIQUE CLUSTERED INDEX SourceAnnotationView_CIX ON dbo.SourceAnnotationView (Author, MemberName, Date, AssemblyName);";

            context.Database.ExecuteSqlCommand(dropView);
            context.Database.ExecuteSqlCommand(createView);
            context.Database.ExecuteSqlCommand(createIndex);

            base.Seed(context);
        }
    }
}

﻿using System;
using System.Reflection;

namespace Xrm.SourceControl.SourceAnalyze.Annotations
{
    /// <summary>
    /// Аннотация позволяющая получить автора изменений/созданий в коде метода/класса
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public sealed class SourceAnnotationAttribute : Attribute
    {
        public SourceAnnotationAttribute(string author, string date, string reason)
        {
            Author = author;
            Date = date;
            Reason = reason;
        }

        public string Author { get; private set; }

        public string Date { get; private set; }

        public string Reason { get; private set; }

        public string TypeName { get; set; }

        public string MemberName { get; set; }

        public string AssemblyName { get; set; }

        public MemberTypes MemberType { get; set; }
    }
}

﻿using System;

namespace Xrm.SourceControl.SourceAnalyze
{
    public interface IAnnotationFactory<TAnnotation, TAnnotationAttribute> where TAnnotationAttribute : Attribute
    {
        TAnnotation Create(TAnnotationAttribute attribute, AttributeTargets info);
    }
}

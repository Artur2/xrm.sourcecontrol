﻿using System;
using System.Collections.Generic;
using Xrm.SourceControl.Data.Models.SourceAnalyze;

namespace Xrm.SourceControl.SourceAnalyze.Annotations
{
    public class SourceAnnotationFactory : IAnnotationFactory<SourceAnnotation, SourceAnnotationAttribute>
    {
        private Dictionary<AttributeTargets, SourceAnnotationMemberType> map = new Dictionary<AttributeTargets, SourceAnnotationMemberType>
        {
            {AttributeTargets.Method, SourceAnnotationMemberType.Method},
            {AttributeTargets.Class, SourceAnnotationMemberType.Class }
        };

        public SourceAnnotation Create(SourceAnnotationAttribute attribute, AttributeTargets target)
        {
            var date = default(DateTime);
            if (!DateTime.TryParse(attribute.Date, out date))
                throw new FormatException(nameof(attribute.Date));

            
            var memberType = default(SourceAnnotationMemberType);
            if (map.ContainsKey(target))
                memberType = map[target];

            return new SourceAnnotation
            {
                AssemblyName = attribute.AssemblyName,
                Author = attribute.Author,
                Date = date,
                MemberName = attribute.MemberName,
                Reason = attribute.Reason,
                MemberType = memberType
            };
        }
    }
}

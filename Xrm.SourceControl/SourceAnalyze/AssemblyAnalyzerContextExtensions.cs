﻿using System;

namespace Xrm.SourceControl.SourceAnalyze
{
    public static class AssemblyAnalyzerContextExtensions
    {
        public static void UseFileAssemblyResolver<TAnnotation,TAnnotationAttribute>(this AssemblyAnalyzerContext<TAnnotation,TAnnotationAttribute> @this,
            string assemblyPath)
            where TAnnotationAttribute : Attribute
        {
            @this.AssemblyResolver = new FileAssemblyResolver(assemblyPath);
        }
    }
}

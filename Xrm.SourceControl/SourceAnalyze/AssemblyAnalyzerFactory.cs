﻿using System;

namespace Xrm.SourceControl.SourceAnalyze
{
    public class AssemblyAnalyzerFactory
    {
        public AssemblyAnalyzer<TAnnotation,TAnnotationAttribute> Create<TAnnotation,TAnnotationAttribute>(Action<AssemblyAnalyzerContext<TAnnotation,TAnnotationAttribute>> configureAction)
            where TAnnotationAttribute : Attribute
        {
            var settings = new AssemblyAnalyzerContext<TAnnotation,TAnnotationAttribute>();
            configureAction(settings);
            return new AssemblyAnalyzer<TAnnotation,TAnnotationAttribute>(settings);
        }
    }
}

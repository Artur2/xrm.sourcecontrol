﻿using System.Reflection;

namespace Xrm.SourceControl.SourceAnalyze
{
    public interface IAssemblyResolver
    {
        Assembly Load();
    }
}

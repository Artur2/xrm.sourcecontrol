﻿using System;
using Xrm.SourceControl.SourceAnalyze.Store;

namespace Xrm.SourceControl.SourceAnalyze
{
    public class AssemblyAnalyzerContext<TAnnotation,TAnnotationAttribute> where TAnnotationAttribute : Attribute
    {
        internal IAssemblyResolver AssemblyResolver { get; set; }

        internal IDataStore<TAnnotation> DataStore { get; set; }

        internal IAnnotationFactory<TAnnotation, TAnnotationAttribute> AnnotationFactory { get; set; }

        public void UseAssemblyResolver<TAssemblyResolver>() where TAssemblyResolver : IAssemblyResolver
        {
            var resolver = Activator.CreateInstance<TAssemblyResolver>();
            AssemblyResolver = resolver;
        }

        public void UseAnnotationFactory<TAnnotationFactory>()
            where TAnnotationFactory : IAnnotationFactory<TAnnotation, TAnnotationAttribute>
        {
            var factory = Activator.CreateInstance<TAnnotationFactory>();
            AnnotationFactory = factory;
        }

        public void UseDataStore<TDataStore>() where TDataStore : IDataStore<TAnnotation>
        {
            var dataStore = Activator.CreateInstance<TDataStore>();
            DataStore = dataStore;
        }

        internal void Validate()
        {
            if (AssemblyResolver == null)
                throw new NullReferenceException("Configure assembly resolver");
            if (DataStore == null)
                throw new NullReferenceException("Configure data store");
            if (AnnotationFactory == null)
                throw new NullReferenceException("Configure annotation factory");
        }
    }
}

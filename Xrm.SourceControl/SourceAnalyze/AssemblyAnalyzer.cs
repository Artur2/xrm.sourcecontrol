﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Xrm.SourceControl.SourceAnalyze
{
    public class AssemblyAnalyzer<TAnnotation, TAnnotationAttribute> where TAnnotationAttribute : Attribute
    {
        private BindingFlags methodAccessFlag = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        public AssemblyAnalyzer(AssemblyAnalyzerContext<TAnnotation, TAnnotationAttribute> context)
        {
            context.Validate();
            Context = context;
        }

        internal AssemblyAnalyzerContext<TAnnotation, TAnnotationAttribute> Context { get; set; }

        public static AssemblyAnalyzerFactory Factory { get; set; } = new AssemblyAnalyzerFactory();

        public IEnumerable<TAnnotation> GetAnnotations(AttributeTargets targets)
        {
            foreach (var tuple in GetAnnotationAttributes(targets))
            {
                yield return Context.AnnotationFactory.Create(tuple.Item1, tuple.Item2);
            }
        }

        public IEnumerable<TAnnotation> InsertNewAnnotations(IEnumerable<TAnnotation> items)
        {
            return Context.DataStore.InsertNewAnnotations(items);
        }

        public IEnumerable<TAnnotation> InsertNewAnnotations(AttributeTargets targets)
        {
            return Context.DataStore.InsertNewAnnotations(GetAnnotations(targets));
        }

        protected IEnumerable<Tuple<TAnnotationAttribute, AttributeTargets>> GetAnnotationAttributes(AttributeTargets targets)
        {
            return GetAnnotationAttributes(targets, typeof(TAnnotationAttribute))
                .Select(tuple => Tuple.Create((TAnnotationAttribute)tuple.Item1, tuple.Item2));
        }

        protected IEnumerable<Tuple<Attribute, AttributeTargets>> GetAnnotationAttributes(AttributeTargets targets, params Type[] attributeTypes)
        {
            if (Context == null)
                throw new NullReferenceException(nameof(Context));

            var assembly = Context.AssemblyResolver.Load();
            if (assembly == null)
                throw new NullReferenceException("assembly is null, please set Assembly property");

            if (attributeTypes == null || !attributeTypes.Any())
                throw new ArgumentNullException("attributeTypes");

            var assemblyTypes = assembly.GetTypes();

            switch (targets)
            {
                case AttributeTargets.Class | AttributeTargets.Method:
                    {
                        foreach (var type in assemblyTypes)
                        {
                            foreach (var attributeType in attributeTypes)
                            {
                                var typeAttribute = type.GetCustomAttribute(attributeType);

                                if (typeAttribute != null)
                                {
                                    yield return new Tuple<Attribute, AttributeTargets>(typeAttribute, AttributeTargets.Class);
                                }

                                foreach (var attribute in GetMethodAttributes(type, attributeType))
                                {

                                    yield return new Tuple<Attribute, AttributeTargets>(attribute, AttributeTargets.Method);
                                }
                            }
                        }
                    }
                    break;
                case AttributeTargets.Class:
                    foreach (var type in assemblyTypes)
                    {
                        foreach (var attributeType in attributeTypes)
                        {
                            var typeAttribute = type.GetCustomAttribute(attributeType);

                            if (typeAttribute == null)
                                continue;

                            yield return new Tuple<Attribute, AttributeTargets>(typeAttribute, AttributeTargets.Class);
                        }
                    }
                    break;
                case AttributeTargets.Method:
                    foreach (var type in assemblyTypes)
                    {
                        var methodMembers = type.GetMembers(methodAccessFlag)
                                    .Where(o => o.MemberType == MemberTypes.Method);

                        foreach (var attributeType in attributeTypes)
                        {
                            foreach (var attribute in GetMethodAttributes(type, attributeType))
                            {
                                yield return new Tuple<Attribute, AttributeTargets>(attribute, AttributeTargets.Method);
                            }
                        }
                    }

                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        private IEnumerable<Attribute> GetMethodAttributes(Type type, Type attributeType)
        {
            var methodMembers = type.GetMembers(methodAccessFlag)
                                    .Where(o => o.MemberType == MemberTypes.Method);

            foreach (var methodMember in methodMembers)
            {
                var methodAttribute = methodMember.GetCustomAttribute(attributeType);
                if (methodAttribute == null)
                    continue;

                yield return methodAttribute;
            }
        }
    }
}

﻿using System.Collections.Generic;

namespace Xrm.SourceControl.SourceAnalyze.Store
{
    public interface IDataStore<T>
    {
        IEnumerable<T> InsertNewAnnotations(IEnumerable<T> annotations);
    }
}

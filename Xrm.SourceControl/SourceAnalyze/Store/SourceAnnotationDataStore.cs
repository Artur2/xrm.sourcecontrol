﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Xrm.SourceControl.Data;
using Xrm.SourceControl.Data.Models.SourceAnalyze;

namespace Xrm.SourceControl.SourceAnalyze.Store
{
    public class SourceAnnotationDataStore : IDataStore<SourceAnnotation>
    {
        public IEnumerable<SourceAnnotation> InsertNewAnnotations(IEnumerable<SourceAnnotation> annotations)
        {
            var existingAnnotations = new List<SourceAnnotation>();
            var newAnnotations = new List<SourceAnnotation>();

            using (var context = new DataContext())
            {
                context.ThrowSeed();

                var query = @"SELECT * 
                                FROM dbo.SourceAnnotationView sav
                                WHERE sav.Author = @Author 
                                AND sav.Date = @Date
                                AND sav.AssemblyName = @AssemblyName
                                AND sav.MemberName = @MemberName";

                var connection = context.Database.Connection;
                using (var transaction = context.Database.BeginTransaction())
                {
                    // Bad thing
                    foreach (var annotation in annotations)
                    {
                        var existingAnnotation = connection.Query<SourceAnnotation>(query, annotation, transaction: transaction.UnderlyingTransaction)
                              .FirstOrDefault();

                        if (existingAnnotation == null)
                        {
                            newAnnotations.Add(annotation);
                            continue;
                        }

                        existingAnnotations.Add(existingAnnotation);
                    }

                    try
                    {
                        foreach (var newAnnotation in newAnnotations)
                        {
                            context.SourceAnnotations.Add(newAnnotation);
                        }

                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("something going wrong");
                    }
                }
            }

            return newAnnotations;
        }
    }
}

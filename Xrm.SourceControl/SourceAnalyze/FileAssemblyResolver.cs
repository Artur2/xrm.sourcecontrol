﻿using System.Reflection;

namespace Xrm.SourceControl.SourceAnalyze
{
    public class FileAssemblyResolver : IAssemblyResolver
    {
        private string _assemblyPath;

        public FileAssemblyResolver(string assemblyPath)
        {
            _assemblyPath = assemblyPath;
        }

        public Assembly Load()
        {
            return Assembly.LoadFile(_assemblyPath);
        }
    }
}

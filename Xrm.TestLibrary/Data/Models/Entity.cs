﻿using System.Diagnostics;
using Xrm.SourceControl.SourceAnalyze.Annotations;

namespace Xrm.TestLibrary.Data.Models
{
    [SourceAnnotation("Artnur","2015-08-23 13:30", "Modified debugger display")]
    [DebuggerDisplay("{Id,nq}")]
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}

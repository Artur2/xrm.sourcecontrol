﻿using System;
using Xrm.SourceControl.SourceAnalyze.Annotations;

namespace Xrm.TestLibrary.Data.Models.Security
{
    [SourceAnnotation("Artnur", "2015-08-22 12:40", "Added new class for Sec module")]
    public class User : Entity
    {
        public string Title { get; set; }

        public string PasswordHash { get; set; }

        [SourceAnnotation("Artnur", "2015-08-22 12:40", "Added new method for Sec module, but not implemented")]
        public void SetPasswordHash(string password)
        {
            throw new NotImplementedException();
        }

        [SourceAnnotation("Artnur", "2015-08-23 13:40", "Added new method for evicting password, but not implemented")]
        public void EvictPassword()
        {
            throw new NotImplementedException();
        }
    }
}
